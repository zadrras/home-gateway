# My personal website

## Obtaining ssl certificate

Using [acme.sh](https://github.com/Neilpang/acme.sh) seems to be the easiest option. Follow [these](https://github.com/Neilpang/acme.sh/wiki/How-to-install) instruction to install it. Afterwards run:

```bash
export DOMAIN=jonasb.dev

acme.sh \
    --issue \
    -d $(DOMAIN) \
    -d www.$(DOMAIN) \
    --standalone \
    --log \
    -k 4096 \
    -ak 4096 \
    --fullchain-file /etc/ssl/private/$(DOMAIN).cer \
    --key-file /etc/ssl/private/$(DOMAIN).key
```

## Nginx hardening

Hardening was done according to [trimstray's guide](https://github.com/trimstray/nginx-quick-reference).
